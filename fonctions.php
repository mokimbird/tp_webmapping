<?php
function age ($date1, $date2 = 'today') {

  if ($date1 == '') {
    return '';
  }

  $jour1 = substr($date1, 0, 2);
  $mois1 = substr($date1, 3, 2);
  $annee1 = substr($date1, 6);
  $time1 = mktime(0, 0, 0, $mois1, $jour1, $annee1);

  if ($date2 === 'today') {
    $time2 = time();
  } else {
    $jour2 = substr($date2, 0, 2);
    $mois2 = substr($date2, 3, 2);
    $annee2 = substr($date2, 6);
    $time2 = mktime(0, 0, 0, $mois2, $jour2, $annee2);
  }

  $ageEnSecondes = abs($time2 - $time1);
  $ageEnJours = $ageEnSecondes / (60 * 60 * 24);

  return floor($ageEnJours);
}
?>
